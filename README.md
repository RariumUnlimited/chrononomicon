Chrononomicon
=============

About
-----

2D Shooter game created using [Unity3D](https://unity.com/)


Build
-----

- The game cannot be built due to missing asset (audio and pathfinding), these assets have not been included due to Unity Asset Store license.

License
-------

All Chrononomicon source code and assets in this repository are provided under the MIT license (See LICENSE file)
