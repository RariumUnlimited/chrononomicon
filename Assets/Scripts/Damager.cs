﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damager : MonoBehaviour {
    public float DamageAmount = 1.0f;

    void OnCollisionEnter2D(Collision2D other) {
        ApplyDamageTo(other.gameObject.GetComponent<Damageable>());
    }

    void OnTriggerStay2D(Collider2D other) {
        ApplyDamageTo(other.GetComponent<Damageable>());
    }

    public void ApplyDamageTo(Damageable other) {
        if (other != null && Util.CheckHostility(gameObject, other.gameObject)) {
            other.ChangeHealth(this);
        }
    }
}
