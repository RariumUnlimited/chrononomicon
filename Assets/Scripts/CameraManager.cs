﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    void Update() {
        Vector2 origin = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x,
                                     Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        RaycastHit2D hit = Physics2D.Raycast(origin, Vector2.zero, 0f);
        if (hit) {
            HoverDialogue dialog = hit.collider.gameObject.GetComponent<HoverDialogue>();
            if (dialog != null) {
                dialog.Display();
            }
        }
    }
}
