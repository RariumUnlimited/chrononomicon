﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponColorLab : MonoBehaviour {
    public GameObject Turret;
    public float ColorStep = 5;
    public TMPro.TMP_Text RedText;
    public TMPro.TMP_Text GreenText;
    public TMPro.TMP_Text BlueText;

    public List<TMPro.TMP_Text> MinusText;
    public List<TMPro.TMP_Text> PlusText;

    public WeaponsIconListUI WeaponsIconListUI;

    private RaycastWeapon weapon;

    void Start() {
        weapon = Turret.GetComponentInChildren<RaycastWeapon>();

        if (weapon == null) {
            Debug.LogError("Unable to find turret raycast weapon in weapon color lab");
        }

        UpdateColorText();

        foreach (TMPro.TMP_Text mT in MinusText) 
            mT.text = "-" + ColorStep.ToString();

        foreach (TMPro.TMP_Text pT in PlusText) 
            pT.text = "+" + ColorStep.ToString();
    }

    private void UpdateColorText() {
        RedText.text = ((int)(weapon.Color.r * 255.0f)).ToString();
        GreenText.text = ((int)(weapon.Color.g * 255.0f)).ToString();
        BlueText.text = ((int)(weapon.Color.b * 255.0f)).ToString();
    }

    public void IncreaseRed(Button b) {
        weapon.Color.r += ColorStep / 255.0f;
        if (weapon.Color.r > 1.0f)
            weapon.Color.r = 1.0f;
        UpdateColorText();
    }

    public void IncreaseGreen(Button b) {
        weapon.Color.g += ColorStep / 255.0f;
        if (weapon.Color.g > 1.0f)
            weapon.Color.g = 1.0f;
        UpdateColorText();
    }

    public void IncreaseBlue(Button b) {
        weapon.Color.b += ColorStep / 255.0f;
        if (weapon.Color.b > 1.0f)
            weapon.Color.b = 1.0f;
        UpdateColorText();
    }

    public void DecreaseRed(Button b) {
        weapon.Color.r -= ColorStep / 255.0f;
        if (weapon.Color.r < 0.0f)
            weapon.Color.r = 0.0f;
        UpdateColorText();
    }

    public void DecreaseGreen(Button b) {
        weapon.Color.g -= ColorStep / 255.0f;
        if (weapon.Color.g < 0.0f)
            weapon.Color.g = 0.0f;
        UpdateColorText();
    }

    public void DecreaseBlue(Button b) {
        weapon.Color.b -= ColorStep / 255.0f;
        if (weapon.Color.b < 0.0f)
            weapon.Color.b = 0.0f;
        UpdateColorText();
    }

    public void Save(Button b) {
        (GetComponent<WeaponLab>().Weapon as RaycastWeapon).Color = weapon.Color;
        GameObject.Find("WeaponsIcons").GetComponent<WeaponsIconListUI>().UpdateColor();
    }

    public void Exit(Button b) {
        GameManager.instance.ReturnToBase();
    }
}
