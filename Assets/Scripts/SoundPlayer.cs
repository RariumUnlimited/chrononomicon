﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance { get; private set; }

    private AudioSource source;

    void Awake() {
        instance = this;
    }
    void Start() {
        source = GetComponent<AudioSource>();
    }

    public void PlayerAudioClip(AudioClip clip) {
        source.PlayOneShot(clip);
    }
}
