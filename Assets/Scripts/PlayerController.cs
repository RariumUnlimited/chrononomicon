﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public float Speed = 4;
    public GameObject BulletPrefab;
    public GameObject LightWeaponTemplate;

    private Rigidbody2D rb;
    private Camera cam;
    private Animator animator;
    private WeaponController weaponController;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        cam = Camera.main;
        weaponController = GetComponent<WeaponController>();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetAxis("Fire1") > 0.5f) {
            animator.SetBool("Firing", true);
            weaponController.FireWeapon(rb.rotation);
        } else {
            animator.SetBool("Firing", false);
        }
        
        if (Input.GetAxis("Mouse ScrollWheel") < 0.0f) {
            weaponController.Weapons.SelectNextWeapon();
        } else if (Input.GetAxis("Mouse ScrollWheel") > 0.0f) {
            weaponController.Weapons.SelectPreviousWeapon();
        } 
    }

    void FixedUpdate() {
        rb.rotation = Util.AngleWithCam(rb.position);

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        
        Vector2 position = rb.position;
        position.x += horizontal * Time.fixedDeltaTime * Speed;
        position.y += vertical * Time.fixedDeltaTime * Speed;
        rb.MovePosition(position);

        animator.SetBool("Moving", horizontal != 0 || vertical != 0);
    }

    public void AddNewLightWeapon(Button button) {
        GameObject lightWeapon = GameObject.Instantiate(LightWeaponTemplate, Vector3.zero, Quaternion.identity);
        lightWeapon.transform.parent = transform;
        Weapon weaponComponent = lightWeapon.GetComponent<Weapon>();
        if (weaponComponent == null) {
            Debug.LogError("Unable to find weapon component in new light weapon");
        }
        weaponController.Weapons.AddWeapon(weaponComponent);
    }
}
