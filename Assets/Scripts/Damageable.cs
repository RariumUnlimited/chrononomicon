﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Damageable : MonoBehaviour {

    [Serializable]
    public class DamageEvent : UnityEvent<Damager, Damageable> { }
    [Serializable]
    public class HealEvent : UnityEvent<Healer, Damageable> { }
    [Serializable]
    public class ResurectEvent : UnityEvent<Damageable> { }

    public float MaxHealth = 100;
    public float Health {
        get {
            return currentHealth;
        }
    }
    public float TimeInvincible = 0.0f;
    public DamageEvent OnDamage;
    public DamageEvent OnDie;
    public ResurectEvent OnResurect;
    public HealEvent OnHeal;
    public AudioClip HitSound;
    public AudioClip DieSound;
    
    private float currentHealth = 100;
    private bool isInvincible;
    private float invincibleTimer;
    
    void Start() {
        Resurect();
    }

    void Update() {
        if (isInvincible) {
            invincibleTimer -= Time.deltaTime;
            if (invincibleTimer < 0)
                isInvincible = false;
        }
    }

    public void Resurect() {
        currentHealth = MaxHealth;
        OnResurect.Invoke(this);
    }

    public void ChangeHealth(Damager damager) {
        if (currentHealth > 0.0f) {
            if (TimeInvincible > 0.0f) {
                if (isInvincible)
                    return;
                
                isInvincible = true;
                invincibleTimer = TimeInvincible;
            }
            currentHealth = Mathf.Clamp(currentHealth - damager.DamageAmount, 0, MaxHealth);

            if (HitSound != null) {
                SoundPlayer.instance.PlayerAudioClip(HitSound);
            }
            OnDamage.Invoke(damager, this);
            if (currentHealth <= 0) {
                if (DieSound != null) {
                    SoundPlayer.instance.PlayerAudioClip(DieSound);
                }
                OnDie.Invoke(damager, this);
            }
        }
    }

    public void ChangeHealth(Healer healer) {
        if (currentHealth > 0.0f) {
            currentHealth = Mathf.Clamp(currentHealth + healer.HealAmount, 0, MaxHealth);
            OnHeal.Invoke(healer, this);
        }
    }
}
