﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponLab : MonoBehaviour {
    public Weapon Weapon;

    public void SetupWeapon(Weapon w) {
        Weapon = w;

        GameObject[] turrets = GameObject.FindGameObjectsWithTag("LabTurret");

        foreach(GameObject t in turrets) {
            LabTurretController ltc = t.GetComponent<LabTurretController>();
            if (ltc == null)
                Debug.LogError("Unable to find lab turret controller in turret");

            ltc.SetWeapon(Weapon);
        }
    }

    public void Exit(Button button) {
        GameManager.instance.ReturnToBase();
    }

    public void NotifyWeaponUpgrade() {
        SetupWeapon(Weapon);
    }
}
