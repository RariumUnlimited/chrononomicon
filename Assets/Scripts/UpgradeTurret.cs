﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeTurret : LabTurretController {
    public WeaponLab WeaponLab;
    public float DamageModifier = 0.0f;
    public float CooldownModifier = 0.0f;
    public float AoEModifier = 0.0f;
    public int PierceModifier = 0;
    public int ForkModifier = 0;
    public int ChainModifier = 0;

    private RaycastWeapon rcWeapon;
    private Damager damager;

    public override void SetWeapon(Weapon w) {
        base.SetWeapon(w);
        rcWeapon = turretWeapon as RaycastWeapon;

        damager = turretWeapon.gameObject.GetComponent<Damager>();

        if (damager == null) {
            Debug.LogError("Unable to find turret damager in upgrade turret");
        }

        damager.DamageAmount += DamageModifier;
        rcWeapon.CooldownTimer -= CooldownModifier;
        if (rcWeapon.AoE == 0.0f && AoEModifier > 0.0f)
            rcWeapon.AoE = rcWeapon.InitialAoE;
        else
            rcWeapon.AoE += AoEModifier;
        rcWeapon.Pierce += PierceModifier;
        rcWeapon.Fork += ForkModifier;
        rcWeapon.Chain += ChainModifier;
    }

    public void ApplyUpgradeToWeapon(Button button) {
        RaycastWeapon rcTurretWeapon = templateWeapon as RaycastWeapon;
        if (DamageModifier != 0.0f)
            turretWeapon.GetComponent<Damager>().DamageAmount = damager.DamageAmount;
        if (CooldownModifier != 0.0f)
            rcTurretWeapon.CooldownTimer = rcWeapon.CooldownTimer;
        if (AoEModifier != 0.0f)
            rcTurretWeapon.AoE = rcWeapon.AoE;
        if (PierceModifier != 0)
            rcTurretWeapon.Pierce = rcWeapon.Pierce;
        if (ForkModifier != 0)
            rcTurretWeapon.Fork = rcWeapon.Fork;
        if (ChainModifier != 0)
            rcTurretWeapon.Chain = rcWeapon.Chain;

        WeaponLab.NotifyWeaponUpgrade();
    }
}
