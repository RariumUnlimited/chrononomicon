﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Util {
    public static float AngleWithCam(Vector3 position) {
        Camera cam = Camera.main;
        float camDis = cam.transform.position.y - position.y;
 
        Vector3 mouse = cam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, camDis));
        
        return AngleWithGameObject(position, mouse);
    }

    public static float AngleWithGameObject(Vector3 position, Vector3 other) {
        float angleRad = Mathf.Atan2 (other.y - position.y, other.x - position.x);
        float angle = (180 / Mathf.PI) * angleRad;
    
        return angle;
    }

    public static Vector2 Rotate(Vector2 vec, float angle) {
        return Quaternion.Euler(0, 0, angle) * vec;
    }

    public static Vector3 Rotate(Vector3 vec, float angle) {
        return Quaternion.Euler(0, 0, angle) * vec;
    }

    public static bool CheckHostility(GameObject a, GameObject b) {
        if (a.tag == "Hostile" || a.tag == "HostileBullet") {
            if (b.tag == "Hostile" || b.tag == "HostileBullet") 
                return false;
            else
                return true;
        } else if (a.tag == "Player" || a.tag == "PlayerBullet") {
            if (b.tag == "Player" || b.tag == "PlayerBullet")
                return false;
            else
                return true;
        }

        return true;
    }
}
