﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour {
    [Serializable]
    public class OnClickEvent : UnityEvent<Button> { }

    public float ClickRepeatDelay = 1.0f;
    public OnClickEvent OnClick;

    private float clickTimer;

    void Start() {
        Damageable damageable = GetComponent<Damageable>();
        if (damageable == null) {
            Debug.LogError("Unable to find Damageable component in Button");
        }
        damageable.OnDie.AddListener(ButtonDied);

        clickTimer = 0.0f;
    }

    void Update() {
        if (clickTimer > 0.0f) {
            clickTimer -= Time.deltaTime;
        }
    }

    private void ButtonDied(Damager damager, Damageable damageable) {
        damageable.Resurect();
        if (clickTimer <= 0.0f) {
            OnClick.Invoke(this);
            clickTimer = ClickRepeatDelay;
        }
    }
}
