﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour {
    public Image Mask;
    private float originalSize;

    void Awake() {
        originalSize = Mask.rectTransform.rect.width;
    }

    public void SetValue(float value) {				      
        Mask.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, originalSize * value);
    }

    public void OnGameObjectDamaged(Damager damager, Damageable damageable) {
        SetValue(damageable.Health / damageable.MaxHealth);
    }

    public void OnGameObjectResurect(Damageable damageable) {
        SetValue(damageable.Health / damageable.MaxHealth);
    }

    public void OnGameObjectHealed(Healer healer, Damageable damageable) {
        SetValue(damageable.Health / damageable.MaxHealth);
    }
}
