﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponIconUI : MonoBehaviour {
    public int WeaponIndex;
    public WeaponController WeaponController;
    public Sprite SelectedSprite;
    public Sprite NotSelectedSprite;

    private Image image;

    void Start() {
        image = GetComponent<Image>();
        if (WeaponController.Weapons.SelectedWeapon == WeaponIndex) {
            image.sprite = SelectedSprite;
        } else {
            image.sprite = NotSelectedSprite;
        }
        WeaponController.Weapons.OnWeaponSelectedChanged.AddListener(OnWeaponSelectionChanged);
    }

    void OnWeaponSelectionChanged(int weaponIndex, Weapon weapon) {
        if (weaponIndex == WeaponIndex) {
            image.sprite = SelectedSprite;
        } else {
            image.sprite = NotSelectedSprite;
        }
    }
}
