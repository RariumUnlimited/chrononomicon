﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FloorContainer : MonoBehaviour
{
    public RectTransform Parent;
    public RectTransform Left;
    public RectTransform Right;
    public bool UpdateSize = false;

    private RectTransform rectTransform;
    void Start() {
        rectTransform = GetComponent<RectTransform>();
        float widthWanted = Parent.rect.width - Left.rect.width - Right.rect.width;
        rectTransform.SetWidth(widthWanted);
    }

    void Update() {
        if (UpdateSize || Application.isEditor) {
            rectTransform = GetComponent<RectTransform>();
            float widthWanted = Parent.rect.width - Left.rect.width - Right.rect.width;
            rectTransform.SetWidth(widthWanted);
        }
    }
}
