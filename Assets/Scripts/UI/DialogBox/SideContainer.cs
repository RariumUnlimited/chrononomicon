﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SideContainer : MonoBehaviour
{
    public RectTransform Parent;
    public RectTransform Top;
    public RectTransform Bottom;
    public bool UpdateSize = false;

    private RectTransform rectTransform;
    void Start() {
        rectTransform = GetComponent<RectTransform>();
        float heightWanted = Parent.rect.height - Top.rect.height - Bottom.rect.height;
        rectTransform.SetHeight(heightWanted);
    }

    void Update() {
        if (UpdateSize || Application.isEditor) {
            rectTransform = GetComponent<RectTransform>();
            float heightWanted = Parent.rect.height - Top.rect.height - Bottom.rect.height;
            rectTransform.SetHeight(heightWanted);
        }
    }
}
