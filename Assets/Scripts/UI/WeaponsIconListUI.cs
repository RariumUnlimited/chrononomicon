﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponsIconListUI : MonoBehaviour {
    public WeaponController WeaponController;
    public GameObject IconTemplate;

    private List<GameObject> icons = new List<GameObject>();

    void Start() {
        for (int i = 0; i < WeaponController.Weapons.Weapons.Count; i++) {
            SetupWeapon(i);
            UpdateColor(i);
        }

        WeaponController.Weapons.OnWeaponAdded.AddListener(OnWeaponAdded);
    }

    void SetupWeapon(int i) {
        GameObject icon = GameObject.Instantiate(IconTemplate);
        icon.transform.SetParent(transform, false);
        

        Image mainImage = icon.GetComponent<Image>();
        if (mainImage == null) 
            Debug.LogError("Unable to find Image component in IconTemplate");
        float height = mainImage.rectTransform.GetHeight();

        RectTransform iconTransform = icon.GetComponent<RectTransform>();
        Vector3 localPosition = iconTransform.localPosition;
        localPosition.y -= height * icons.Count;
        iconTransform.localPosition = localPosition;

        WeaponIconUI wsUi = icon.GetComponent<WeaponIconUI>();
        if (wsUi == null) 
            Debug.LogError("Unable to find WeaponIconUI component in IconTemplate");
        wsUi.WeaponController = WeaponController;
        wsUi.WeaponIndex = i;

        Image iconImage = icon.transform.GetChild(1).GetComponent<Image>();
        if (iconImage == null) 
            Debug.LogError("Unable to find Image component in IconTemplate child icon");
        iconImage.sprite = WeaponController.Weapons.Weapons[i].Icon;

        icons.Add(icon);
    }

    void UpdateColor(int i) {
        GameObject icon = icons[i];

        Image rayIconImage = icon.transform.GetChild(0).GetComponent<Image>();
        if (rayIconImage == null) 
            Debug.LogError("Unable to find Image component in IconTemplate child ray icon");
        rayIconImage.color = (WeaponController.Weapons.Weapons[i] as RaycastWeapon).Color;

        Image iconImage = icon.transform.GetChild(1).GetComponent<Image>();
        if (iconImage == null) 
            Debug.LogError("Unable to find Image component in IconTemplate child icon");
        iconImage.color = (WeaponController.Weapons.Weapons[i] as RaycastWeapon).Color;
    }

    void OnWeaponAdded(int i, Weapon weapon) {
        SetupWeapon(i);
    }

    public void UpdateColor() {
        for (int i = 0; i < icons.Count; i++) {
            UpdateColor(i);
        }
    }
}
