﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialDialog : MonoBehaviour {
    public GameObject NextTutorial;

    public void EndTutorial() {
        if (NextTutorial != null) {
            GameObject nextTutorial = GameObject.Instantiate(NextTutorial, Vector3.zero, Quaternion.identity);
            TutorialDialog tutoDiag = nextTutorial.GetComponent<TutorialDialog>();
            if (tutoDiag == null) {
                Debug.LogError("Unable to find TutorialDialog in next tutorial");
            }
        } else {
            GameManager.instance.ReturnToBase();
        }

        Destroy(gameObject);
    }
}
