﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial2 : TutorialDialog {
    private bool ShootingGood = false;
    //private bool ChangingWeapon = false;

    void Update() {
        if (Input.GetAxis("Fire1") > 0.0f) {
            ShootingGood = true;
        }

        /*if (Input.GetAxis("Mouse ScrollWheel") != 0.0f) {
            ChangingWeapon = true;
        }*/

        if (ShootingGood) {
            EndTutorial();
        }
    }
}
