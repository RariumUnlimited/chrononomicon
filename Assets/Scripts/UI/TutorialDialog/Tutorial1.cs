﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial1 : TutorialDialog {
    private bool leftGood = false;
    private bool rightGood = false;
    private bool downGood = false;
    private bool upGood = false;

    void Update() {
        if (Input.GetAxis("Vertical") < 0.0f) {
            downGood = true;
        }

        if (Input.GetAxis("Vertical") > 0.0f) {
            upGood = true;
        }

        if (Input.GetAxis("Horizontal") < 0.0f) {
            leftGood = true;
        }

        if (Input.GetAxis("Horizontal") > 0.0f) {
            rightGood = true;
        }

        if (downGood && upGood && leftGood && rightGood) {
            EndTutorial();
        }
    }
}
