﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial3 : TutorialDialog
{
    public GameObject EndTutorialButton;
    void Start() {
        EndTutorialButton.GetComponent<Damageable>().OnDie.AddListener(OnButtonClick);
    }

    void OnButtonClick(Damager damager, Damageable damageable) {
        EndTutorial();
    }
}
