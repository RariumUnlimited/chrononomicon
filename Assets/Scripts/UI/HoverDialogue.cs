﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverDialogue : MonoBehaviour {
    public GameObject DialogBox;
    public float DisplayTime = 1.0f;

    private float displayTimer = -1.0f;

    void Start() {
        DialogBox.SetActive(false);

    }

    // Update is called once per frame
    void Update() {
        if (displayTimer >= 0) {
            displayTimer -= Time.deltaTime;
            if (displayTimer < 0) {
                DialogBox.SetActive(false);
            }
        }
    }

    public void Display() {
        displayTimer = DisplayTime;
        DialogBox.SetActive(true);
    }
}
