﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float LaunchForce = 1.0f;
    public AudioClip HitSound;
    public GameObject Explosion;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Awake() {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update() {
        
    }

    public void Launch(Vector2 direction) {
        rb.AddForce(direction.normalized * LaunchForce);
    }

    void OnCollisionEnter2D(Collision2D other) {
        //we also add a debug log to know what the projectile touch
        //Debug.Log("Projectile Collision with " + other.gameObject);
        if (HitSound != null) {
            SoundPlayer.instance.PlayerAudioClip(HitSound);
        }

        if (Explosion != null) {
            Instantiate(Explosion, other.GetContact(0).point, Quaternion.identity);
        }
        
        Destroy(gameObject);
    }
}
