﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletWeapon : Weapon {
    public GameObject BulletPrefab;

    protected override void DoShoot(Transform mountPosition, float angle) {
        GameObject bulletObject = Instantiate(BulletPrefab, 
                                                mountPosition.position,
                                                Quaternion.Euler(0, 0, angle));
                                                Bullet bullet = bulletObject.GetComponent<Bullet>();

        if (bullet != null) {
            Vector2 launchDirection = Util.Rotate(Vector2.right, angle);
            bullet.Launch(launchDirection);
        } else {
            Debug.LogError("Unable to find Bullet component in bullet");
        }
    }
}
