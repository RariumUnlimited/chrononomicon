﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastWeapon : Weapon
{
    public float AoE = 0.0f;
    public int Pierce = 0;
    public int Fork = 0;
    public int Chain = 0;
    public float ChainRange = 1.0f;
    public GameObject Explosion;
    public GameObject RayLine;
    public float RayDelay = 0.1f;
    public Color Color;
    public float InitialAoE { get { return initialAoE; } }

    private float initialAoE = 0.25f;
    private float forkAngle = 45.0f;
    private float forkAngleFactor = 0.66f;

    private struct ForkResult {
        public RaycastHit2D Fork1Hit;
        public RaycastHit2D Fork2Hit;
        public Vector2 Fork1Direction;
        public Vector2 Fork2Direction;
        public bool Fork1HitWall;
        public bool Fork2HitWall;
    }

    protected override void DoShoot(Transform mountPosition, float angle) {
        RaycastHit2D lastHit = new RaycastHit2D();
        bool hitWall = ProcessPierce(mountPosition.position, angle, out lastHit);

        if (hitWall)
            return;

        List<RaycastHit2D> forks = new List<RaycastHit2D>();
        List<Vector2> directions = new List<Vector2>();
        forks.Add(lastHit);
        directions.Add(Util.Rotate(Vector2.right, angle));

        int remainingFork = Fork;

        while (remainingFork > 0) {
            List<RaycastHit2D> newForks = new List<RaycastHit2D>();
            List<Vector2> newDirections = new List<Vector2>();

            for (int i = 0; i < forks.Count; i++) {
                ForkResult result = ProcessFork(forks[i], directions[i], Fork - remainingFork);

                if (!result.Fork1HitWall) {
                    newForks.Add(result.Fork1Hit);
                    newDirections.Add(result.Fork1Direction);
                }
                
                if (!result.Fork2HitWall) {
                    newForks.Add(result.Fork2Hit);
                    newDirections.Add(result.Fork2Direction);
                }
            }

            forks = newForks;
            directions = newDirections;

            remainingFork--;
        }

        if (Chain > 0) 
            foreach (RaycastHit2D hit in forks)
                ProcessChain(hit);
    }

    private void ProcessChain(RaycastHit2D hit2D) {
        int remainingChain = Chain;
        LayerMask layers = LayerMask.GetMask("Environment", "Hostile");

        HashSet<int> chains = new HashSet<int>();

        RaycastHit2D lastHit = hit2D;
        chains.Add(lastHit.collider.gameObject.GetInstanceID());

        while (remainingChain > 0) {
            List<Collider2D> list = new List<Collider2D>(Physics2D.OverlapCircleAll(lastHit.point, ChainRange, layers));

            list.Sort(delegate(Collider2D a, Collider2D b) {
                float distanceToA = (a.gameObject.transform.position - lastHit.collider.gameObject.transform.position).sqrMagnitude;
                float distanceToB = (b.gameObject.transform.position - lastHit.collider.gameObject.transform.position).sqrMagnitude;

                return distanceToA.CompareTo(distanceToB);
            });

            Collider2D closest = null;
            RaycastHit2D closestHit = new RaycastHit2D();

            for (int i = 0; i < list.Count; i++) {
                if (list[i].gameObject.CompareTag("Wall"))
                    continue;

                if (chains.Contains(list[i].gameObject.GetInstanceID()))
                    continue;

                if (list[i].gameObject.GetInstanceID() == lastHit.collider.gameObject.GetInstanceID())
                    continue;

                bool good = false;

                List<RaycastHit2D> hits = new List<RaycastHit2D>();

                Vector2 direction = list[i].gameObject.transform.position;
                direction -= lastHit.point;

                int hitCount = Raycast(lastHit.point, direction.normalized, hits);

                if (hitCount < 2)
                    continue;

                for (int j = 0; j < hitCount; j++) {
                    if (hits[j].collider.gameObject.GetInstanceID() == lastHit.collider.gameObject.GetInstanceID())
                        continue;

                    if (hits[j].collider.gameObject.GetInstanceID() == list[i].gameObject.GetInstanceID()) {
                        closestHit = hits[j];
                        good = true;
                    }
                    break;
                }

                if (!good)
                    continue;

                chains.Add(list[i].gameObject.GetInstanceID());
                closest = list[i];
                break;
            }

            if (closest == null) {
                remainingChain = 0;
            } else {
                Detonate(lastHit.point, closestHit);

                lastHit = closestHit;

                remainingChain--;
            }
        }
    }

    private float GetForkAngleFactor(int step) {
        if (step == 0)
            return 1.0f;
        else
            return GetForkAngleFactor(step - 1) * forkAngleFactor;
    }

    private ForkResult ProcessFork(RaycastHit2D hit2D, Vector2 direction, int step) {
        ForkResult result = new ForkResult();

        float angleFactor = GetForkAngleFactor(step);

        result.Fork1Direction = Util.Rotate(direction, forkAngle * angleFactor);
        result.Fork2Direction = Util.Rotate(direction, -forkAngle * angleFactor);

        List<RaycastHit2D> hits = new List<RaycastHit2D>();
        int hitCount = Raycast(hit2D.point, result.Fork1Direction, hits);

        for (int i = 0; i < hitCount; i++) {
            if (hits[i].collider.gameObject.GetInstanceID() == hit2D.collider.gameObject.GetInstanceID())
                continue;

            Detonate(hit2D.point, hits[i]);

            result.Fork1Hit = hits[i];
            result.Fork1HitWall = hits[i].collider.gameObject.CompareTag("Wall");
            
            break;
        }

        hitCount = Raycast(hit2D.point, result.Fork2Direction, hits);

        for (int i = 0; i < hitCount; i++) {
            if (hits[i].collider.gameObject.GetInstanceID() == hit2D.collider.gameObject.GetInstanceID())
                continue;

            Detonate(hit2D.point, hits[i]);

            result.Fork2Hit = hits[i];
            result.Fork2HitWall = hits[i].collider.gameObject.CompareTag("Wall");
            
            break;
        }


        return result;
    }

    private bool ProcessPierce(Vector2 mountPosition, float angle, out RaycastHit2D lastHit) {
        List<RaycastHit2D> hits = new List<RaycastHit2D>();

        int hitCount = Raycast(mountPosition, Util.Rotate(Vector2.right, angle), hits);

        int remainingPierce = 1 + Pierce;

        lastHit = new RaycastHit2D();

        lastHit.point = mountPosition;

        bool hitWall = false;

        for (int i = 0; i < hitCount && remainingPierce > 0; i++) {
            remainingPierce--;

            Detonate(lastHit.point, hits[i]);

            if (hits[i].collider.gameObject.CompareTag("Wall")) {
                remainingPierce = 0;
                hitWall = true;
            }

            lastHit = hits[i];
        }

        return hitWall;
    }

    private int Raycast(Vector2 origin, Vector2 direction, List<RaycastHit2D> hit2Ds) {
        LayerMask layers = LayerMask.GetMask("Environment", "Hostile");

        ContactFilter2D filter2D = new ContactFilter2D();
        filter2D.useLayerMask = true;
        filter2D.SetLayerMask(layers);

        return Physics2D.Raycast(origin, direction, filter2D, hit2Ds, Mathf.Infinity);
    }

    private void Detonate(Vector3 startPosition, RaycastHit2D hitInfo) {
        if (Explosion != null) {
            GameObject explosion = GameObject.Instantiate(Explosion, hitInfo.point, Quaternion.identity);
            if (AoE > 0.0f)
                explosion.transform.localScale = new Vector3(AoE / InitialAoE, AoE / InitialAoE);
            SpriteRenderer exploRenderer = explosion.GetComponent<SpriteRenderer>();
            exploRenderer.color = Color;
        }

        if (RayLine != null) {
            GameObject rayLine = Instantiate(RayLine);
            LineRenderer line = rayLine.GetComponent<LineRenderer>();
            line.SetPosition(0, startPosition);
            line.SetPosition(1, hitInfo.point);
            line.startColor = Color;
            line.endColor = Color;
            Destroy(rayLine,RayDelay);
        }

        if (AoE > 0.0f) {
            LayerMask layers = LayerMask.GetMask("Environment", "Hostile");
            List<Collider2D> list = new List<Collider2D>(Physics2D.OverlapCircleAll(hitInfo.point, AoE, layers));
            foreach (Collider2D collider in list) 
                GetComponent<Damager>().ApplyDamageTo(collider.gameObject.GetComponent<Damageable>());
        } else {
            GetComponent<Damager>().ApplyDamageTo(hitInfo.transform.GetComponent<Damageable>());
        }
    }
}
