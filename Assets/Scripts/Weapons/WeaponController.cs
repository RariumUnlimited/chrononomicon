﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class WeaponController : MonoBehaviour
{
    [Serializable]
    public class WeaponList {
        [Serializable]
        public class SelectedWeaponEvent : UnityEvent<int, Weapon> { }
        public List<Weapon> Weapons;
        public int SelectedWeapon = 0;
        public SelectedWeaponEvent OnWeaponSelectedChanged;
        public SelectedWeaponEvent OnWeaponAdded;

        public void SelectNextWeapon() {
            if (Weapons.Count > 0) {
                SelectedWeapon++;
                if (SelectedWeapon >= Weapons.Count) {
                    SelectedWeapon = 0;
                }
                OnWeaponSelectedChanged.Invoke(SelectedWeapon, Weapons[SelectedWeapon]);
            }
        }

        public void SelectPreviousWeapon() {
            if (Weapons.Count > 0) {
                SelectedWeapon--;
                if (SelectedWeapon < 0) {
                    SelectedWeapon = Weapons.Count - 1;
                }
            }
            OnWeaponSelectedChanged.Invoke(SelectedWeapon, Weapons[SelectedWeapon]);
        }

        public void SelectWeapon(int weapon) {
            weapon = Mathf.Clamp(weapon, 0, Weapons.Count);
            OnWeaponSelectedChanged.Invoke(SelectedWeapon, Weapons[SelectedWeapon]);
        }

        public void AddWeapon(Weapon weapon) {
            int newIndex = Weapons.Count;
            Weapons.Add(weapon);
            OnWeaponAdded.Invoke(newIndex, weapon);
        }
    }

    public WeaponList Weapons;

    public GameObject[] Mounts;
    private float weaponTimer;

    void Start() {
        weaponTimer = 0.0f;
    }

    void Update() {
        if (weaponTimer > 0.0f) {
            weaponTimer -= Time.deltaTime;
        }
    }

    public void FireWeapon(float shootAngle) {
        if (Weapons.Weapons.Count > 0 && weaponTimer <= 0.0f) {
            Weapon selected = Weapons.Weapons[Weapons.SelectedWeapon];
            weaponTimer = selected.CooldownTimer;
            for(int i = 0; i < Mounts.Length; i++) {
                selected.Shoot(Mounts[i].transform, shootAngle);
            }
        }
    }

    public Weapon GetSelectedWeapon() {
        return Weapons.Weapons[Weapons.SelectedWeapon];
    }
}
