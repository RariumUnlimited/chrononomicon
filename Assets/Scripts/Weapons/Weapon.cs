﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {
    public float CooldownTimer = 1.0f;
    public AudioClip ShootSound;
    public Sprite Icon;

    public void Shoot(Transform mountPosition, float angle) {
        if (ShootSound != null) {
            SoundPlayer.instance.PlayerAudioClip(ShootSound);
        }

        DoShoot(mountPosition, angle);
    }

    protected abstract void DoShoot(Transform mountPosition, float angle);
}
