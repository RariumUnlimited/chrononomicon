﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LabTurretController : MonoBehaviour {
    protected Weapon templateWeapon;
    protected Weapon turretWeapon = null;

    private WeaponController weaponController;

    void Awake() {
        weaponController = GetComponent<WeaponController>();
        if (weaponController == null) {
            Debug.LogError("Unable to find weapon controller in lab turret");
        }
    }

    void Update() {
        weaponController.FireWeapon(transform.rotation.z * 180.0f);
    }

    public virtual void SetWeapon(Weapon w) {
        templateWeapon = w;

        if (turretWeapon != null)
            Destroy(turretWeapon);
        else
            Debug.Log("Prout");

        weaponController.Weapons.Weapons.Clear();

        turretWeapon = GameObject.Instantiate(templateWeapon.gameObject).GetComponent<Weapon>();
        turretWeapon.transform.parent = transform;
        weaponController.Weapons.AddWeapon(turretWeapon);
    }
}
