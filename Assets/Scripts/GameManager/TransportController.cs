﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportController : MonoBehaviour {

    public GameObject TransportAnimation;
    public float TransportDuration = 3.0f;
    public delegate void LevelLoadedCallback(GameObject level);

    private bool transporting = false;
    private bool transportingLevelSetup = false;
    private float transportTimer;
    private GameObject transportAnimationPlaying;

    private GameObject currentLevel;
    private GameObject destinationLevel;
    private bool doingTutorial = false;
    private LevelLoadedCallback callback = null;


    private GameManager gm;
    private GameObject transportingPlayer;

    void Start() {
        gm = GetComponent<GameManager>();
        if (gm == null) {
            Debug.LogError("Unable to find game manager in transport controller");
        }

        if (gm.FirstTutorial != null && !gm.PlayerData.TutorialDone) {
            gm.Base.gameObject.SetActive(false);
            GameObject firstTutorial = GameObject.Instantiate(gm.FirstTutorial, Vector3.zero, Quaternion.identity);
            doingTutorial = true;
        }
    }

    void Update() {
        if (transporting) {
            transportTimer -= Time.deltaTime;
            if (transportTimer < TransportDuration * 0.25f && !transportingLevelSetup) {
                if (currentLevel != null) {
                    Destroy(currentLevel);
                    currentLevel = null;
                }
                if (destinationLevel != null) {
                    gm.Base.gameObject.SetActive(false);
                }
                if (destinationLevel == null) {
                    gm.Base.gameObject.SetActive(true);
                }

                transportingPlayer.transform.position = Vector3.zero;
                transportAnimationPlaying.transform.position = Vector3.zero;
                SetupLevel();
            } 
            
            if (transportTimer < 0) {
                transportingPlayer.GetComponent<PlayerController>().enabled = true;
                transporting = false;
                Destroy(transportAnimationPlaying);
            }
        }
    }

    private void SetupLevel() {
        if (destinationLevel != null) {
            currentLevel = GameObject.Instantiate(destinationLevel, Vector3.zero, Quaternion.identity);
            if (callback != null)
                callback(currentLevel);
        }
        
        transportingLevelSetup = true;
    }

    public bool TransportPlayer(GameObject wantedLevel, GameObject player, LevelLoadedCallback levelLoadedCallback = null) {
        if (!transporting) {
            transportingPlayer = player;
            transportAnimationPlaying = GameObject.Instantiate(TransportAnimation, player.transform.position, Quaternion.identity);
            player.GetComponent<PlayerController>().enabled = false;
            player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            transporting = true;
            transportingLevelSetup = false;
            transportTimer = TransportDuration;
            destinationLevel = wantedLevel;

            // Tutorial management
            if (wantedLevel == null && doingTutorial) {
                gm.PlayerData.TutorialDone = true;
                SaveSystem.SavePlayerData(gm.PlayerData);
            }

            callback = levelLoadedCallback;

            return true;
        } else {
            return false;
        }
    }
}
