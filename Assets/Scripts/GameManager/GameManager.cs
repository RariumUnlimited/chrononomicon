﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour {
    [Serializable]
    public class EndGameEvent : UnityEvent<GameManager, bool> { }

    public static GameManager instance { get; private set; }

    public Base Base;
    public PlayerData PlayerData;
    public EndGameEvent OnGameEnd;
    public List<GameObject> Levels;
    public GameObject FirstTutorial;
    public GameObject WeaponColorLab;
    public GameObject WeaponUpgradeLab;
    public GameObject Player;

    private int hostileRemaining;
    private int currentLevelIndex;
    private TransportController transportController;
    private TransportController.LevelLoadedCallback weaponLabLoadedcallback;

    void Awake() {
        instance = this;

        PlayerData data = SaveSystem.LoadPlayerData();
        if (data != null) {
            PlayerData = data;
        } else {
            PlayerData = new PlayerData();
        }

        weaponLabLoadedcallback = WeaponLabLoadedcallback;
    }

    void Start() {
        transportController = GetComponent<TransportController>();

        currentLevelIndex = 0;

        Player.GetComponent<Damageable>().OnDie.AddListener(OnPlayerDie);
        Base.MainText.text = "Welcome !";
    }

    public void ReturnToBase() {
        if (transportController.TransportPlayer(null, Player)) {   
            Player.GetComponent<Damageable>().Resurect();
            currentLevelIndex = Levels.Count;
        }
    }

    public void RegisterHostileDefeated(GameObject hostile) {
        hostileRemaining--;
        if (hostileRemaining == 0) {
            OnGameEnd.Invoke(this, true);

            if (currentLevelIndex == Levels.Count - 1) {
                Base.MainText.text = "Victory !";

                ReturnToBase();
            } else {
                currentLevelIndex++;
                if (transportController.TransportPlayer(Levels[currentLevelIndex], Player)) {
                    hostileRemaining = Levels[currentLevelIndex].GetComponent<Level>().HostileDefeatedCondition;
                }
            }
        }
    }

    public void NewGame(Button button) {
        currentLevelIndex = 0;
        if (transportController.TransportPlayer(Levels[currentLevelIndex], Player)) {
            hostileRemaining = Levels[currentLevelIndex].GetComponent<Level>().HostileDefeatedCondition;
        }
    }

    public void ToWeaponColorLab(Button button) {
        transportController.TransportPlayer(WeaponColorLab, Player, weaponLabLoadedcallback);
    }

    public void ToWeaponUpgradeLab(Button button) {
        transportController.TransportPlayer(WeaponUpgradeLab, Player, weaponLabLoadedcallback);
    }

    private void OnPlayerDie(Damager damager, Damageable damageable) {
        OnGameEnd.Invoke(this, false);
        Base.MainText.text = "Defeat...";

        ReturnToBase();
    }

    private void WeaponLabLoadedcallback(GameObject level) {
        level.GetComponent<WeaponLab>().SetupWeapon(Player.GetComponent<WeaponController>().GetSelectedWeapon());
    }
}
