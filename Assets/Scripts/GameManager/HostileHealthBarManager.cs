﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileHealthBarManager : MonoBehaviour {
    public GameObject HostileHealthBar;
    public HealthBarUI HostileHealthBarUI;

    void Update() {
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, Mathf.Infinity, LayerMask.GetMask("MouseOverDetector"));
 
        if(hit.collider != null) {
            Damageable damageable = hit.collider.gameObject.GetComponentInParent<Damageable>();
            if (damageable != null && hit.collider.gameObject.GetComponentInParent<Button>() == null) {
                HostileHealthBar.SetActive(true);
                HostileHealthBarUI.SetValue(damageable.Health / damageable.MaxHealth);
            } else {
                HostileHealthBar.SetActive(false);
            }
        } else {
                HostileHealthBar.SetActive(false);
        }
    }
}
