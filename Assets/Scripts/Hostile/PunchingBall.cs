﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchingBall : MonoBehaviour {
    private Damageable damageable;

    void Start() {
        damageable = GetComponent<Damageable>();
        if (damageable == null)
            Debug.LogError("Unable to find damageable in punching ball");
        damageable.OnDie.AddListener(Resurect);
    }

    void Resurect(Damager damager, Damageable damageable) {
        damageable.Resurect();
    }
}
