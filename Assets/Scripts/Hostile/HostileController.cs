﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileController : MonoBehaviour {
    public bool UseWeapon = false;
    public float FireRange;

    private Damageable damageable;
    private WeaponController weaponController;

    void Start() {
        damageable = GetComponent<Damageable>();
        damageable.OnDie.AddListener(Died);
        weaponController = GetComponent<WeaponController>();
        GameManager.instance.OnGameEnd.AddListener(Freeze);
    }

    public void Update() {
        if (UseWeapon && weaponController != null) {
            Collider2D targetCollider = Physics2D.OverlapCircle(transform.position, FireRange, LayerMask.GetMask("Player"));

            //float distanceToTarget = Vector2.Distance(transform.position, Target.transform.position);
            if (targetCollider != null) {
                GameObject target = targetCollider.gameObject;
                float distanceToTarget = Vector2.Distance(transform.position, target.transform.position);

                LayerMask layers = LayerMask.GetMask("Environment");

                RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, target.transform.position - transform.position, distanceToTarget, layers);
                if (hitInfo.collider == null)
                    weaponController.FireWeapon(Util.AngleWithGameObject(transform.position, target.transform.position));
            }
        }
    }

    void Freeze(GameManager gameManager, bool victory) {
        UseWeapon = false;
        GetComponent<PolyNav.PolyNavAgent>().Stop();
    }

    void Died(Damager damager, Damageable damageable) {
        GameManager.instance.RegisterHostileDefeated(gameObject);
        Destroy(gameObject);
    }
}
