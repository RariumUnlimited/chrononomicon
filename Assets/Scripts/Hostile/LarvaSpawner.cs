﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LarvaSpawner : MonoBehaviour {
    public GameObject Larva;
    public float SpawnDelay = 0.0f;
    public float SpawnInterval = 4.0f;
    public int SpawnCount = 3;
    private GameObject player;
    private float nextSpawn;
    private int spawnRemaining;

    void Start() {
        player = GameObject.Find("Player");
        if (player == null) 
            Debug.LogError("Spawner is unable to find Player");

        nextSpawn = SpawnDelay;
        spawnRemaining = SpawnCount;
    }

    void Update() {
        nextSpawn -= Time.deltaTime;
        if (nextSpawn <= 0.0f) {
            if (spawnRemaining > 0) {
                nextSpawn = SpawnInterval;

                GameObject larva = GameObject.Instantiate(Larva, transform.position, Quaternion.identity);
                spawnRemaining--;

                larva.transform.parent = transform;

                FollowTarget followTarget = larva.GetComponent<FollowTarget>();
                if (followTarget != null) {
                    followTarget.target = player.transform;
                }
            }
        }
    }
}
